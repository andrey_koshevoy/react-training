import {ConverterCreator}  from './converterCreator'
import {ConverterWidget} from './components/converter/converterWidget';

ReactDOM.render(
    <ConverterWidget converters={new ConverterCreator().getAllConverters()}/>,
     document.getElementById('root')
     );