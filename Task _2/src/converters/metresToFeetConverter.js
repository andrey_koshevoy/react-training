import { ConverterBase } from './converterBase'

const FROM = 'm';
const TO = 'ft';
export class MetresToFeetConverter extends ConverterBase{
    constructor(){
        super();
    }

    convert(...args){
        var parsed = super.convert(args);
        var func = (el) => el * 3.2808399;
        return parsed.map(el => func(el));
    }

    isMatch(unitFrom, unitTo){
       return unitFrom == FROM && unitTo == TO;
    }
    
    toString(){
        return "Metres -> Feet";
    }
}


