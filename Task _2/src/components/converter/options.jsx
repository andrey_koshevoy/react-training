export class Options extends React.Component{
    render(){
        var rows = [];
        rows.push(<option key={-1} value={-1} disabled> -- select an option -- </option>)
        this.props.converters.forEach((converter, index)=>{
            rows.push(<option key={index} value={index}>{converter.toString()}</option>)
        });
        return(
            <div>
            <label>Select converter</label>
            <select className="form-control" onChange={this.props.handleConverterChange} value={this.props.index}>
               {rows}
            </select>
            </div>
            );
    }
        
}