export class Result extends React.Component{
    render(){
        if(this.props.converter === undefined || !this.props.value)
        {
            return(
            <div className="panel panel-warning" style={{marginTop:20 + 'px'}}>
                <div className="panel-heading">Result</div>
                <div className="panel-body">Please chose converter and set value</div>
            </div>
            );
        }
        if(isNaN(this.props.value))
        {
            return(
            <div className="panel panel-danger" style={{marginTop:20 + 'px'}}>
                <div className="panel-heading">Result</div>
                <div className="panel-body">Please enter a valid number</div>
            </div>
            );
        }
        return(
            <div className="panel panel-success" style={{marginTop:20 + 'px'}}>
                <div className="panel-heading">Result</div>
                <div className="panel-body">{ this.props.converter.convert(this.props.value) }</div>
            </div>
            );
    }
        
}