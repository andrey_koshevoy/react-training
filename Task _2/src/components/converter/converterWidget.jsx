import {Options} from './options';
import {Value} from './value';
import {Result} from './result';

export class ConverterWidget extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            chosenConverterIndex : -1,
            value: NaN
        };
    }
    
    handleConverterChange(evt){
        this.setState({chosenConverterIndex : evt.target.value});
    }

    handleValueChange(evt){
        this.setState({value : evt.target.value});
    }

    render(){
        return(
            <div className="form-group">
                <Options converters={this.props.converters} index={this.state.chosenConverterIndex} handleConverterChange={this.handleConverterChange.bind(this)}/>
                <Value isSelected={this.state.chosenConverterIndex == -1} handleValueChange={this.handleValueChange.bind(this)}/>
                <Result converter={this.props.converters[this.state.chosenConverterIndex]} value={this.state.value}/>
            </div>
        );
    }
}