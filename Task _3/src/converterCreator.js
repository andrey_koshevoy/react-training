import {FahrenheitToCelsiusConverter} from './converters/fahrenheitToCelsiusConverter';
import {MetresToFeetConverter} from './converters/metresToFeetConverter';
import {KilogramsToGramsConverter} from './converters/kilogramsToGramsConverter';

export class ConverterCreator{
    constructor(){
        var temperatureConverters = new Array();
        temperatureConverters.push(new FahrenheitToCelsiusConverter());

        var lengthConverters = new Array();
        lengthConverters.push(new MetresToFeetConverter());
        
        var massConverters = new Array();
        massConverters.push(new KilogramsToGramsConverter());

        this.converters = new Array();
        this.converters["Temperature"] = temperatureConverters;
        this.converters["Length"] = lengthConverters;
        this.converters["Mass"] = massConverters;
    }

    createConverter(from, to){
        var converter = this.converters.find(el => {
           if(el.isMatch(from, to))
           {
               return el;
           }
        });
        if(converter === undefined)
        {
            throw new RangeError(`converter FROM unit ${from} TO unit ${to} not found`); 
        }
        return converter;
    }

    getAllConverters(){
        return this.converters;
    }
}