export class Value extends React.Component{
    render(){
        return(
            <div>
                <label>Set value</label>
                <div className="input-group">
                    <input type="text" className="form-control" onChange={this.props.handleValueChange} disabled={this.props.isSelected}/>
                </div>
            </div>
            );
    }
        
}