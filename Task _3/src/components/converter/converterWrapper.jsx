import {ConverterWidget}  from './converterWidget'
import { Link } from 'react-router';

export default class ConverterWrapper extends React.Component {
    render() {
        return (
            <div>
                <p><Link to={'/'}>Home</Link></p>
                <ConverterWidget converters={this.props.route.converters[this.props.params.convertersKey]}/>
            </div>
        );
    }
}