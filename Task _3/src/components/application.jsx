import { Link } from 'react-router';
import {ConverterCreator}  from '../converterCreator'

export default class Application extends React.Component {
    render() {
        var links = [];
        for(var key in this.props.route.converters){
            links.push(<li key={key}><Link to={{pathname:key, converter:this.props.route.converters[key]}}>{key}</Link></li>);
        }
        var routes = (
            <div>
                <h1>Available converters</h1>
                <ul>{links}</ul>
            </div>
        );
        return (
            <div>
                {this.props.children 
                    ? this.props.children
                    : routes} 
            </div>
            
        );
    }
}
