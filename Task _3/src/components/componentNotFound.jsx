export default class ComponentNotFound extends React.Component {
    render() {
        return (
            <h2>Not found!</h2>
        );
    }
}