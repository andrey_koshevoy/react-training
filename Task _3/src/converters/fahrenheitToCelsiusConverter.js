import { ConverterBase } from './converterBase'

const FROM = 'F';
const TO = 'C';
export class FahrenheitToCelsiusConverter extends ConverterBase{
    constructor(){
        super();
    }

    convert(...args){
        var parsed = super.convert(args);
        var func = (el) => (el - 32)/1.8;
        return parsed.map(el => func(el));
    }

    isMatch(unitFrom, unitTo){
       return unitFrom == FROM && unitTo == TO;
    }
    
    toString(){
       return "Fahrenheit -> Celsius";
    }
}


