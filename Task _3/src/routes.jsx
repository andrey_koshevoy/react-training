import React, { Component } from 'react';
import { Router, Route, browserHistory } from 'react-router';
import { ConverterCreator } from './converterCreator';
import ConverterWrapper from './components/converter/converterWrapper';
import Application  from './components/application';
import ComponentNotFound from './components/componentNotFound';

export default class Routes extends Component {
    render() {
        var converters = new ConverterCreator().getAllConverters();
        return (
            <Router history={browserHistory}>
                <Route path="/" component={Application} converters={converters}>
                    <Route path="/:convertersKey" component={ConverterWrapper} converters={converters}/>
                </Route>
            </Router>
        );
    }
}