'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.ConverterCreator = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _fahrenheitToCelsiusConverter = require('./converters/fahrenheitToCelsiusConverter');

var _metresToFeetConverter = require('./converters/metresToFeetConverter');

var _kilogramsToGramsConverter = require('./converters/kilogramsToGramsConverter');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ConverterCreator = exports.ConverterCreator = function () {
    function ConverterCreator() {
        _classCallCheck(this, ConverterCreator);

        this.converters = new Array();
        this.converters.push(new _fahrenheitToCelsiusConverter.FahrenheitToCelsiusConverter());
        this.converters.push(new _metresToFeetConverter.MetresToFeetConverter());
        this.converters.push(new _kilogramsToGramsConverter.KilogramsToGramsConverter());
    }

    _createClass(ConverterCreator, [{
        key: 'createConverter',
        value: function createConverter(from, to) {
            var converter = this.converters.find(function (el) {
                if (el.isMatch(from, to)) {
                    return el;
                }
            });
            if (converter === undefined) {
                throw new RangeError('converter FROM unit ' + from + ' TO unit ' + to + ' not found');
            }
            return converter;
        }
    }]);

    return ConverterCreator;
}();