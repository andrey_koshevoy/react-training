'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _converterCreator = require('./converterCreator');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Runner = function () {
    function Runner() {
        _classCallCheck(this, Runner);
    }

    _createClass(Runner, null, [{
        key: 'main',
        value: function main() {
            var fahrenheitToCelsiusConverter = new _converterCreator.ConverterCreator().createConverter('F', 'C');
            console.log(fahrenheitToCelsiusConverter.convert(10));
            console.log(fahrenheitToCelsiusConverter.convert(10, 11, 12));
            console.log(fahrenheitToCelsiusConverter.convert("20"));
            console.log(fahrenheitToCelsiusConverter.convert([10, 20, "23"]));

            var metersToFeetConverter = new _converterCreator.ConverterCreator().createConverter('m', 'ft');
            console.log(metersToFeetConverter.convert(10));

            var kilogramsToGramsConverter = new _converterCreator.ConverterCreator().createConverter('kg', 'g');
            console.log(kilogramsToGramsConverter.convert(1));
        }
    }]);

    return Runner;
}();

Runner.main();