'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.KilogramsToGramsConverter = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _converterBase = require('./converterBase');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var FROM = 'kg';
var TO = 'g';

var KilogramsToGramsConverter = exports.KilogramsToGramsConverter = function (_ConverterBase) {
    _inherits(KilogramsToGramsConverter, _ConverterBase);

    function KilogramsToGramsConverter() {
        _classCallCheck(this, KilogramsToGramsConverter);

        return _possibleConstructorReturn(this, (KilogramsToGramsConverter.__proto__ || Object.getPrototypeOf(KilogramsToGramsConverter)).call(this));
    }

    _createClass(KilogramsToGramsConverter, [{
        key: 'convert',
        value: function convert() {
            for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
                args[_key] = arguments[_key];
            }

            var parsed = _get(KilogramsToGramsConverter.prototype.__proto__ || Object.getPrototypeOf(KilogramsToGramsConverter.prototype), 'convert', this).call(this, args);
            var func = function func(el) {
                return el * 1000;
            };
            return parsed.map(function (el) {
                return func(el);
            });
        }
    }, {
        key: 'isMatch',
        value: function isMatch(unitFrom, unitTo) {
            return unitFrom == FROM && unitTo == TO;
        }
    }, {
        key: 'toString',
        value: function toString() {
            console.log("KilogramsToGramsConverter");
        }
    }]);

    return KilogramsToGramsConverter;
}(_converterBase.ConverterBase);