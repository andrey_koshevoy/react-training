"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ConverterBase = exports.ConverterBase = function () {
    function ConverterBase() {
        _classCallCheck(this, ConverterBase);

        if (new.target === ConverterBase) {
            throw new TypeError("Cannot construct Abstract instances directly");
        }

        if (this.convert === undefined) {
            throw new TypeError("Must override method convert");
        }

        if (this.isMatch === undefined) {
            throw new TypeError("Must override method isMatch");
        }
    }

    _createClass(ConverterBase, [{
        key: "convert",
        value: function convert() {
            var toNumberFunc = function toNumberFunc(el) {
                var casted = parseFloat(el);
                if (casted !== undefined) {
                    return casted;
                }
            };

            for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
                args[_key] = arguments[_key];
            }

            return flatten(args).map(function (el) {
                if (Array.isArray(el)) {
                    return el.map(function (inner) {
                        return toNumberFunc(inner);
                    });
                } else {
                    return toNumberFunc(el);
                }
            });
        }
    }, {
        key: "isMatch",
        value: function isMatch(unitFrom, unitTo) {
            return false;
        }
    }]);

    return ConverterBase;
}();

function flatten(arr) {
    return arr.reduce(function (flat, toFlatten) {
        return flat.concat(Array.isArray(toFlatten) ? flatten(toFlatten) : toFlatten);
    }, []);
}