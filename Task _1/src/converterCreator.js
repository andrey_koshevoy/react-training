import {FahrenheitToCelsiusConverter} from './converters/fahrenheitToCelsiusConverter';
import {MetresToFeetConverter} from './converters/metresToFeetConverter';
import {KilogramsToGramsConverter} from './converters/kilogramsToGramsConverter';

export class ConverterCreator{
    constructor(){
        this.converters = new Array();
        this.converters.push(new FahrenheitToCelsiusConverter());
        this.converters.push(new MetresToFeetConverter());
        this.converters.push(new KilogramsToGramsConverter());
    }

    createConverter(from, to){
        var converter = this.converters.find(el => {
           if(el.isMatch(from, to))
           {
               return el;
           }
        });
        if(converter === undefined)
        {
            throw new RangeError(`converter FROM unit ${from} TO unit ${to} not found`); 
        }
        return converter;
    }
}