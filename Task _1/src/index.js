import {ConverterCreator}  from './converterCreator'

class Runner{
    static main()
    {
        var fahrenheitToCelsiusConverter = new ConverterCreator().createConverter('F', 'C');
        console.log(fahrenheitToCelsiusConverter.convert(10));
        console.log(fahrenheitToCelsiusConverter.convert(10,11,12));
        console.log(fahrenheitToCelsiusConverter.convert("20"));
        console.log(fahrenheitToCelsiusConverter.convert([10, 20, "23"]));

        var metersToFeetConverter = new ConverterCreator().createConverter('m', 'ft');
        console.log(metersToFeetConverter.convert(10));

        var kilogramsToGramsConverter = new ConverterCreator().createConverter('kg', 'g');
        console.log(kilogramsToGramsConverter.convert(1));
    }
}

Runner.main();