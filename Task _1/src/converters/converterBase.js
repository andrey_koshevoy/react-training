export class ConverterBase{
    constructor(){
        if (new.target === ConverterBase) {
            throw new TypeError("Cannot construct Abstract instances directly");
        }

        if (this.convert === undefined) {
            throw new TypeError("Must override method convert");
        }

        if (this.isMatch === undefined) {
            throw new TypeError("Must override method isMatch");
        }
    }

    convert(...args){
        var toNumberFunc = (el) =>{
            var casted = parseFloat(el);
            if(casted !== undefined)
            {
                return casted;
            }
        }
        return flatten(args).map(el => {
            if(Array.isArray(el)){
                return el.map(inner => toNumberFunc(inner));
            }
            else{
                return toNumberFunc(el);
            }
        });
    }

    isMatch(unitFrom, unitTo){
        return false;
    }
}

function flatten(arr) {
  return arr.reduce(function (flat, toFlatten) {
    return flat.concat(Array.isArray(toFlatten) ? flatten(toFlatten) : toFlatten);
  }, []);
}
