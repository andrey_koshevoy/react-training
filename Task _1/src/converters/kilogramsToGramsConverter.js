import { ConverterBase } from './converterBase'

const FROM = 'kg';
const TO = 'g';
export class KilogramsToGramsConverter extends ConverterBase{
    constructor(){
        super();
    }

    convert(...args){
        var parsed = super.convert(args);
        var func = (el) => el * 1000;
        return parsed.map(el => func(el));
    }

    isMatch(unitFrom, unitTo){
       return unitFrom == FROM && unitTo == TO;
    }
    
    toString(){
        console.log("KilogramsToGramsConverter");
    }
}


